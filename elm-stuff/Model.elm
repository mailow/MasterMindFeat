module Model exposing
( Color(..)
  Model(..)
)

type alias Color =
  {clicked : Bool
  , required : Bool
  , path : String
  }

blue =
  {clicked = False
  , required = True
  , path = "assets/blue.png"
  }

grey =
  {clicked = False
  , required = True
  , path = "assets/grey.png"
  }

green =
  {clicked = False
  , required = True
  , path = "assets/green.png"
  }

orange =
  {clicked = False
  , required = True
  , path = "assets/orange.png"
  }

red =
  {clicked = False
  , required = False
  , path = "assets/red.png"
  }

type alias Model =
  { blue : Color
  , grey : Color
  , green : Color
  , orange : Color
  , red : Color
  , test : String
  }

optOut : Model
optOut =
  Model blue grey green orange red "test"

